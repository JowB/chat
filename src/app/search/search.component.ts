import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RoomService} from '../Services/room.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  searchValue: string;

  constructor(private formBuilder: FormBuilder, private room: RoomService) { }

  ngOnInit() {
  }

  Search(search) {
    this.searchValue = search.value;
    this.room.setFilter(search.value);
    this.room.emitFilter();
  }
}
