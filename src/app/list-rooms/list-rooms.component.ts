import {Component, DoCheck, OnInit} from '@angular/core';
import {RoomService} from '../Services/room.service';

@Component({
  selector: 'app-list-rooms',
  templateUrl: './list-rooms.component.html',
  styleUrls: ['./list-rooms.component.css']
})
export class ListRoomsComponent implements OnInit {

  private rooms: any;

  constructor(private roomService: RoomService) { }

  ngOnInit() {
    this.roomService.roomsSubject.subscribe(
      (data) => this.rooms = data
    );
    this.roomService.getRooms();

    this.roomService.filterSubject.subscribe(
      (data) => this.rooms = data
    );
  }
}
