import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MessageService} from '../Services/message.service';
import {Router} from '@angular/router';
import {AuthentificationService} from '../Services/authentification.service';

@Component({
  selector: 'app-add-message',
  templateUrl: './add-message.component.html',
  styleUrls: ['./add-message.component.css']
})
export class AddMessageComponent implements OnInit {

  addMessageForm: FormGroup;
  message: any = {};
  @Input() room_id: number;

  constructor(private formBuilder: FormBuilder, private messageService: MessageService,
              private router: Router, private authService: AuthentificationService) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.addMessageForm = this.formBuilder.group({
      content: ['', Validators.required]
    });
  }

  addMessage() {
    const content = this.addMessageForm.get('content').value;
    this.message['content'] = content;
    this.message['user_id'] = this.authService.id;
    this.messageService.addMessage(this.message, this.room_id);
    this.addMessageForm.reset();
  }
}
