import {Component, OnInit} from '@angular/core';
import {RoomService} from '../Services/room.service';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css']
})
export class RoomComponent implements OnInit {

  private room: any;
  private id: number;

  constructor(private route: ActivatedRoute, private roomService: RoomService, private router: Router) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.onRouteChange();
      }
    });
  }

  onRouteChange() {
    this.roomService.roomSubject.subscribe(
      (data) => {
        this.room = data;
      }
    );
    this.id = Number(this.route.snapshot.params['id']);
    this.roomService.getRoomById(this.id);
  }

  ngOnInit() {
    this.roomService.roomSubject.subscribe(
      (data) => this.room = data
    );
    this.id = Number(this.route.snapshot.params['id']);
    this.roomService.getRoomById(this.id);
  }
}
