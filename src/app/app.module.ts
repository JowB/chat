import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { InscriptionComponent } from './inscription/inscription.component';
import {RouterModule} from '@angular/router';
import {appRoutes} from './route';
import {RoomService} from './Services/room.service';
import {MessageService} from './Services/message.service';
import {AuthentificationService} from './Services/authentification.service';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ListRoomsComponent } from './list-rooms/list-rooms.component';
import { RoomComponent } from './room/room.component';
import { HomeComponent } from './home/home.component';
import { AddMessageComponent } from './add-message/add-message.component';
import { AddRoomComponent } from './add-room/add-room.component';
import {AuthGuard} from './auth.guard';
import { RegisterComponent } from './register/register.component';
import { SearchComponent } from './search/search.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    InscriptionComponent,
    ListRoomsComponent,
    RoomComponent,
    HomeComponent,
    RegisterComponent,
    SearchComponent,
    HomeComponent,
    AddMessageComponent,
    AddRoomComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    RoomService,
    MessageService,
    AuthentificationService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
