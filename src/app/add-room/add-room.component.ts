import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RoomService} from '../Services/room.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-room',
  templateUrl: './add-room.component.html',
  styleUrls: ['./add-room.component.css']
})
export class AddRoomComponent implements OnInit {

  addForm: FormGroup;
  room: any = {};

  constructor(private formBuilder: FormBuilder, private roomService: RoomService, private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.addForm = this.formBuilder.group({
      name: ['', Validators.required]
    });
  }

  addRoom() {
    const name = this.addForm.get('name').value;
    this.room['name'] = name;
    this.roomService.addRoom(this.room);
    this.router.navigate(['']);
  }
}
