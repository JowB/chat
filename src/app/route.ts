import {Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {HomeComponent} from './home/home.component';
import {AuthGuard} from './auth.guard';
import {RegisterComponent} from './register/register.component';
import {AddRoomComponent} from './add-room/add-room.component';

export const appRoutes: Routes = [
  { path: '', canActivate: [AuthGuard], component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'room/:id', canActivate: [AuthGuard], component: HomeComponent },
  { path: 'room/new', component: AddRoomComponent}
  ];
