import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Subject} from 'rxjs/Subject';
import {RoomService} from './room.service';

@Injectable()
export class MessageService {

  messageSubject: Subject<any> = new Subject<any>();
  message: any;
  messages: any = [];
  constructor(private http: HttpClient, private roomService: RoomService) { }

  emitMessage() {
    this.messageSubject.next(this.message);
  }

  addMessage(message, room_id) {
    console.log(room_id);
    this.http.post<any>('http://192.168.1.36/api-chat/public/' + room_id + '/message/new', message).subscribe(
      (data) => {
        this.roomService.room['messages'] = data;
        this.roomService.emitRoomById();
      },
      (error) => console.log(error)
    );
  }

}
