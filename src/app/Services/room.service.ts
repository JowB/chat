import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Subject} from 'rxjs/Subject';
import * as url from 'url';

@Injectable()
export class RoomService {

  roomsSubject: Subject<any> = new Subject<any>();
  roomSubject: Subject<any> = new Subject<any>();
  filterSubject: Subject<any> = new Subject<any>();
  private rooms_url: url = 'http://192.168.1.36/api-chat/public/room';
  private room_byid = 'http://192.168.1.36/api-chat/public/room/';
  private add_room = 'http://192.168.1.36/api-chat/public/room/new';
  private rooms: any = [];
  public room: any;
  public filter: string = '';

  constructor(private http: HttpClient) { }

  emitRooms() {
    this.roomsSubject.next(this.rooms);
  }

  getRooms() {
    this.http.get<any>(this.rooms_url).subscribe(
      (data) => {
        this.rooms = data;
        this.emitRooms();
      },
      (error) => console.log(error)
    );
  }

  addRoom(room) {
    return this.http.post<any>(this.add_room, room).subscribe(
      (data) => {
        this.rooms.push({
          name: data.room_name,
          id: data.room_id
        });
        this.emitRooms();
      },
      (error) => console.log(error)
    );
  }

  emitRoomById() {
    this.roomSubject.next(this.room);
  }

  getRoomById($id) {
    this.http.get<any>(this.room_byid + $id).subscribe(
      (data) => {
        console.log(data);
        this.room = data;
        this.emitRoomById();
      },
      (error) => console.log(error),
    );
  }

  emitFilter() {
    this.filterSubject.next(this.getFilteredResults());
  }

  setFilter(filter: string) {
    this.filter = filter;
  }

  getFilteredResults() {
    return this.rooms.filter(room => room.name.toLowerCase().includes(this.filter.toLowerCase()) );
  }
}
