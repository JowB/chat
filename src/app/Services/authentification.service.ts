import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Router} from '@angular/router';

@Injectable()
export class AuthentificationService {

  public isAuth: boolean = false;
  public pseudo: string;
  public id: number;

  private login_url = 'http://192.168.1.36/api-chat/public/user/connect';
  private register_url = 'http://192.168.1.36/api-chat/public/user/new';
  constructor(private http: HttpClient, private route: Router) { }

  onLogin(user) {
    let observable = this.http.post(this.login_url, user); // pipe(share...)
    observable.subscribe(
      (data) => {
        if (data['response'] === true) {
        this.isAuth = true;
        this.pseudo = data['user_name'];
        this.id = data['user_id'];
        }
      },
        (error) => console.log(error),
      () => console.log('complete')
    );
    return observable;
  }

  onRegister(user) {
    let observable = this.http.post(this.register_url, user); // pipe(share...)
    observable.subscribe(
      (data) => console.log(data),
      (error) => console.log(error),
      () => console.log('complete')
    );
    return observable;
  }

}
