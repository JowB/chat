import {Component, OnInit} from '@angular/core';
import {AuthentificationService} from '../Services/authentification.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from '../Models/user';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  postForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private route: Router, private authService: AuthentificationService) {
  }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.postForm = this.formBuilder.group({
      pseudo: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  login() {
    const pseudo = this.postForm.get('pseudo').value;
    const password = this.postForm.get('password').value;
    const user = new User(pseudo, password);
    this.authService.onLogin(user).subscribe(
      (data) => this.route.navigate(['']),
      (error) => console.log(error),
      () => console.log('complete')
    );
  }
}
