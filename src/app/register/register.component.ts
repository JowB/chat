import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthentificationService} from '../Services/authentification.service';
import {Router} from '@angular/router';
import {User} from '../Models/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  postFormRegister: FormGroup;

  constructor(private formBuilder: FormBuilder, private route: Router, private authService: AuthentificationService) {}

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.postFormRegister = this.formBuilder.group({
      pseudo: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  register() {
    const pseudo = this.postFormRegister.get('pseudo').value;
    const password = this.postFormRegister.get('password').value;
    const user = new User(pseudo, password);
    this.authService.onRegister(user).subscribe(
      (data) => this.route.navigate(['login']),
      (error) => console.log(error),
      () => console.log('complete')
    );
  }
}
